import { FETCH_NEWS_BEGIN, FETCH_NEWS_FAILURE, FETCH_NEWS_SUCCESS } from '../constants/ActionTypes';
import { URL } from '../constants/constants'

const fetchNewsRequest = () => ({
    type: FETCH_NEWS_BEGIN
  }
);

const fetchNewsSuccess = (payload) => ({
    type: FETCH_NEWS_SUCCESS,
    payload
  }
);

const fetchNewsError = () => ({
    type: FETCH_NEWS_FAILURE
  }
);

export const fetchNews = () => ((dispatch) => {
  	dispatch(fetchNewsRequest());
    return fetch(URL, { method: 'GET'})
    .then( response => response.json()).then((response) =>{
      dispatch(fetchNewsSuccess(response))
    }).catch((error) => {
      dispatch(fetchNewsError())
    })
  });