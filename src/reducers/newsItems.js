import { FETCH_NEWS_BEGIN, FETCH_NEWS_SUCCESS } from '../constants/ActionTypes';

export default function newsItems(state = [], action) {
  switch (action.type) {
  case FETCH_NEWS_BEGIN:
      return state;
  case FETCH_NEWS_SUCCESS: 
      return [...state, ...action.payload];
  default:
    return state;
  }
}