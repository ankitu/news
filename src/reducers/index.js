import { combineReducers } from 'redux';
import newsItems from './newsItems';

const rootReducer = combineReducers({
  newsItems
});

export default rootReducer;