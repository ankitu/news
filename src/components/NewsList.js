import React, {useState} from 'react';
import News from './News'
import '../scss/NewsList.scss'

const NewsList = (props) => {
  const [expanded, setValue] = useState(false);
  const extraNews = props.sub.map((news) => {
    return <News 
    key={news.id}
    class="extra-news"
    title={news.title}
    source={news.source}
    timestamp={news.timestamp}>
  </News>
  });
  return (
    <div className="news-list">
      <News
        class="main-news"
        title={props.title}
        source={props.source}
        timestamp={props.timestamp}
      />
      {expanded ? extraNews : extraNews.splice(0, 1)}
      <button
        className={expanded ? "collapse" : ""}
        onClick={() => setValue(!expanded)}
      >
        &gt;
      </button>
    </div>
  );
}

export default NewsList;