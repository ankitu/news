import React from 'react'
import Time from './Time'
import '../scss/News.scss';

const News = (props) => {
  return (
    <div className={props.class}>
      <h4 className="title">{props.title}</h4>
      <span className="source">{props.source}</span>
      <Time className="timestamp" timestamp={props.timestamp}></Time>
    </div>
  );
}

export default News;