import '../scss/App.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NewsList from '../components/NewsList';
import * as NewsActions from '../actions/NewsActions';

class App extends Component {

  componentDidMount() {
    this.props.actions.fetchNews();
  }

  render() {
    const { newsItems } = this.props;
    console.log(this.props, newsItems);
    const newsList = newsItems.map(function(news, index){
      return <NewsList key={index} {...news}></NewsList>;
    });
    return (
      <section className="app-container">
        {newsList}
      </section>
    );
  }
}

App.propTypes = {
  newsItems: PropTypes.array,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    newsItems: state.newsItems
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(NewsActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
