var express = require('express');
var cors = require('cors');
const fs = require('fs');

var app = express();
var webpack = require("webpack");
var webpackDevMiddleware = require("webpack-dev-middleware");
var webpackHotMiddleware = require("webpack-hot-middleware");
var config = require("./webpack.dev.js");
var compiler = webpack(config);

app.use(webpackHotMiddleware(compiler));
app.use(
  webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  })
);

app.set('port', (process.env.PORT || 3000)); //gets port from environment or else defaults to 3000

app.use(express.static(__dirname + '/dist')); //read files from this folder

app.get('/', function(req, res) { //request, response
  res.render('index.html');
});

app.get('/news', cors(), (req, res) => {
  fs.readFile('./lib/news.json', (err, json) => {
      let obj = JSON.parse(json);
      res.json(obj);
  });
});

app.get('*', function (req, res) {
  res.redirect('/');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
